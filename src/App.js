import React from 'react';
import './App.css';
import Chatroom from './component/chatroom/Chatroom';


function App() {
  return (
    <div className="App">
      <Chatroom></Chatroom>
    </div>
  );
}

export default App;
