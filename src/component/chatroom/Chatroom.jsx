import React, {Component, Fragment} from 'react';
import Message from './Message';


class Chatroom extends Component{
    constructor(props){
        super(props)
        this.state = {
            data: [],
            id_user: Date.now(),
            count: 0
        }
        this.t = 0
        this.msg = React.createRef()
        this.submitMessage = this.submitMessage.bind(this)
    }

    componentDidMount(){
        // this.scrollToBot();
        this.connectionWs();
        
    }

    connectionWs = () => {
        const ws = new WebSocket(`ws://localhost:8000/api/v.01/ws/${this.state.id_user}`)
        ws.onmessage = this.onmessage

        this.setState({
            ws: ws
        })
    }

    notAnyReaction = ()=>{
        const {data, count} = this.state;
        
        const bot_by = {
            username: "Bot",
            content: "If you have any more questions, don't hesitate to contact me again....",
            index: count + 1,
            img: "https://icon-library.com/images/customer-service-icon-png/customer-service-icon-png-27.jpg"
        }
        
        let cad_data = [...data]
        
        cad_data.push(bot_by)

        this.setState({
            data: cad_data,
            count: count + 1
        })
       
        clearTimeout(this.t)
    }

    componentWillUnmount(){
        const { ws } = this.state;
        ws.close()
    }

    onmessage = (env) => {
        
        const recv = JSON.parse(env.data);
        const {data, count} = this.state;

        let dataChat = [...data]

        if(count > 20){
            dataChat = dataChat.slice(1)
        }

        dataChat.push({username: recv.username, content: recv.content, index: count, img: recv.img})

        this.setState({
            data: dataChat,
            count: count + 1
        })
    }

    submitMessage = (e) => {
        e.preventDefault();
        const {ws} = this.state;
        const chat_us = JSON.stringify({
            username: this.state.id_user,
            content: this.msg.current.value, 
        })
        ws.send(chat_us)
        this.msg.current.value = ""

        this.t = setTimeout(this.notAnyReaction, 100000);
    }

    render(){
        return(
            <Fragment>
                <div className="chatroom">
                    <h3>Chattime</h3>
                    <ul className="chats">
                        {
                            this.state.data.map((chat) => {
                                return <Message key={chat.index} chat={chat} user={this.state.id_user}/>
                            })
                        }
                    </ul>
                    <form className="input" onSubmit={(e) => this.submitMessage(e)}>
                        <input type="text" ref={this.msg}/>
                        <input type="submit" value="Submit"/>
                    </form>
                </div>
            </Fragment>
        )
    }
}


export default Chatroom;