import React from 'react';


function Message(props){
    return(
        <li className={`chat ${props.user === props.chat.username ? "right" : "left"}`}>
            {props.user !== props.chat.username
            && <img src={props.chat.img} alt={`${props.chat.username}'s profile pic`} />
            }
            {props.chat.content}
        </li>
    )
};

export default Message;